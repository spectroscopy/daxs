import numpy as np
import pytest

from daxs.utils.arrays import merge


def test_merge_invalid():
    with pytest.raises(ValueError):
        merge(np.array([0.3, 0.4]), np.array([0.5, 0.6]), mode="invalid")


def test_merge_no_overlap():
    a = np.array([0.4, 0.5])
    b = np.array([0.6, 0.7])
    assert merge(a, b, mode="intersection") == pytest.approx([])
    assert merge(a, b, mode="union") == pytest.approx([0.4, 0.5, 0.6, 0.7])


def test_merge_empty_arrays():
    a = np.array([0.4, 0.5])
    b = np.array([0.45])
    assert merge(a, b, mode="intersection") == pytest.approx([0.45])
    assert merge(a, b, mode="union") == pytest.approx([0.4, 0.45, 0.5])


def test_merge_one_common_element():
    a = np.array([0.4, 0.5])
    b = np.array([0.5, 0.6])
    assert merge(a, b, mode="intersection") == pytest.approx([0.5])
    assert merge(a, b, mode="union") == pytest.approx([0.4, 0.5, 0.6])


def test_merge_multiple_common_elements():
    a = np.array([0.3, 0.4, 0.5])
    b = np.array([0.45, 0.5])
    assert merge(a, b, mode="intersection") == pytest.approx([0.45, 0.5])
    assert merge(a, b, mode="union") == pytest.approx([0.3, 0.4, 0.45, 0.5])

    a = np.array([0.3, 0.4, 0.5])
    b = np.array([0.3, 0.4, 0.5, 0.6])
    assert merge(a, b, mode="intersection") == pytest.approx([0.3, 0.4, 0.5])
    assert merge(a, b, mode="union") == pytest.approx([0.3, 0.4, 0.5, 0.6])

    a = np.array([0.3, 0.4, 0.5])
    b = np.array([0.35, 0.55, 0.75])
    assert merge(a, b, mode="intersection") == pytest.approx([0.35, 0.4, 0.5])
    assert merge(a, b, mode="union") == pytest.approx([0.3, 0.35, 0.4, 0.5, 0.55, 0.75])

    a = np.array([0.35, 0.4, 0.75])
    b = np.array([0.2, 0.3, 0.4])
    assert merge(a, b, mode="intersection") == pytest.approx([0.35, 0.4])
    assert merge(a, b, mode="union") == pytest.approx([0.2, 0.3, 0.35, 0.4, 0.75])

    a = np.array([0.3, 0.4, 0.5, 0.6, 0.7])
    b = np.array([0.3, 0.5, 0.7])
    assert merge(a, b, mode="intersection") == pytest.approx([0.3, 0.4, 0.5, 0.6, 0.7])
    assert merge(a, b, mode="union") == pytest.approx([0.3, 0.4, 0.5, 0.6, 0.7])

    a = np.array([0, 2, 4])
    b = np.array([1, 2, 3])
    assert merge(a, b, mode="intersection") == pytest.approx([1, 2, 3])
    assert merge(a, b, mode="union") == pytest.approx([0, 1, 2, 3, 4])
