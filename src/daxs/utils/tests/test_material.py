from __future__ import annotations

import pytest

from daxs.utils.material import MATERIALS, Material

MATERIALS.update({"material1": [1.1, 2.2, 3.3, 45.6, 56.7, 78.9]})


@pytest.mark.parametrize("material_name, volume", (("Si", 160.165818),))
def test_material(material_name: str, volume: float):
    material = Material(*MATERIALS[material_name], name=material_name)
    assert material.volume == pytest.approx(volume)


@pytest.mark.parametrize(
    "material_name, reflection, d_spacing",
    (
        ("Ge", (3, 3, 3), 1.088882),
        ("diamond", (1, 0, 0), 3.567),
        ("LiNbO3", (1, 0, -4), 1.0372142),
        ("material1", (1, 1, 1), 0.871592),
    ),
)
def test_calculate_d_spacing(
    material_name: str, reflection: tuple[int], d_spacing: float
):
    material = Material.from_name(material_name)
    assert material.calculate_d_spacing(reflection) == pytest.approx(d_spacing)
