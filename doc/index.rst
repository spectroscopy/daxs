Data Analysis for X-ray Spectroscopy 
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks/index
   modules/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
