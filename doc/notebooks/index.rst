Notebooks
=========

.. toctree::
   :maxdepth: 2

   xas_separate_scans
   xes_outliers_removal
   xes_concentration_correction
   rixs_concentration_correction
