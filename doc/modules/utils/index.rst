:mod:`daxs.utils`: Utility functions
------------------------------------

.. toctree::
   :maxdepth: 1

   arrays
   bragg
   material
